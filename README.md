# 准备工作


[行走中华·逐梦名企校网通知](http://today.hitwh.edu.cn/news_show.asp?id=27580)
[关于开展2017年暑期“三下乡”社会实践的通知](http://today.hitwh.edu.cn/news_show.asp?id=27472)
[2017年“三下乡”官网报备流程](http://sxx.youth.cn/zytz/sxxtz/201706/t20170613_10055116.htm)
这里记录的是**2017年“行走中华?逐梦名企”暑期社会实践专项活动，上海苏州地区**的准备工作列表。
[保险投保页面](http://www.tianan-life.com/product/toProductDetail/8a8087b451513bbc01515784298500b1)

|tasks|states|details|create-date|finish-date|负责人|
|-|-|-|-|-|-|
|成员集中|未完成|建好群组，收集联系方式|2017-07-10||张楠|
|立项申请表|未完成|填写好立项申请表，部分内容由赵老师完成|2017-07-10||张楠|
|买保险|未完成|为每个队员购买保险，保险时间在8月|2017-07-10||张楠|
|报备“三下乡”社会实践出征授旗仪式|未完成|[2017年“三下乡”官网报备流程](http://sxx.youth.cn/zytz/sxxtz/201706/t20170613_10055116.htm) [校网通知](http://today.hitwh.edu.cn/news_show.asp?id=27472)|2017-07-10||张楠|
|准备“三下乡”社会实践出征授旗仪式|未完成|不需要负责旗帜|2017-07-10||张楠|
